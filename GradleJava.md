
# Wie erstelle ich ein Java-Projekt mit Gradle?

Legen Sie in einem Verzeichnis Ihrer Wahl einen Ordner an mit einem Namen, der das Projekt treffend beschreibt. Mit dem Windows-Kommando `mkdir` (_make directory_) erstellt man einen Ordner (man spricht auch von einem Verzeichnis), und mit dem Kommando `cd` (_change directory_) wechselt man in das angegebene Verzeichnis.

~~~ shell
C:\Users\Dominikus\GitTHM\pis\SoSe2019>mkdir tictactoe

C:\Users\Dominikus\GitTHM\pis\SoSe2019>cd tictactoe

C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe>
~~~

Gradle hilft einem bei der Initialisierung eines Projektverzeichnisses.

~~~ shell
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe>gradle init

Select type of project to generate:
  1: basic
  2: cpp-application
  3: cpp-library
  4: groovy-application
  5: groovy-library
  6: java-application
  7: java-library
  8: kotlin-application
  9: kotlin-library
  10: scala-library
Enter selection (default: basic) [1..10] 6

Select build script DSL:
  1: groovy
  2: kotlin
Enter selection (default: groovy) [1..2] 1

Select test framework:
  1: junit
  2: testng
  3: spock
Enter selection (default: junit) [1..3] 1

Project name (default: tictactoe):
Source package (default: tictactoe):

BUILD SUCCESSFUL in 31s
2 actionable tasks: 2 executed
~~~

Gradle hat einige Dateien angelegt. Wichtig für Sie ist vor allem die angelegte Ordnerstruktur für Ihre Quelldateien. Sie sehen, dass Gradle zwei Beispieldateien angelegt hat. (Das Kommando `dir` listet die Inhalte eines Verzeichnisses, _directory_, auf. Die Option `/s` zeigt auch den Inhalt der Unterverzeichnisse an, `/b` stellt die Auflistung kompakt zusammen.)

~~~ shell
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe>dir src /s /b
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe\src\main
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe\src\test
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe\src\main\java
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe\src\main\resources
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe\src\main\java\tictactoe
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe\src\main\java\tictactoe\App.java
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe\src\test\java
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe\src\test\resources
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe\src\test\java\tictactoe
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe\src\test\java\tictactoe\AppTest.java
~~~

> Wenn Sie mit Git arbeiten, denken Sie daran, eine .gitignore-Datei anzulegen! All die automatisch und temporär angelegten Dateien, die Gradle erzeugt (wie z.B. die kompilierten Java-Dateien), haben nichts in einem Git-Repository verloren. Git dient hauptsächlich zur Verwaltung des Quellcodes in `/src`!

Wenn Sie mögen, können Sie die Beispieldatei kompilieren und ausführen lassen.

~~~ shell
C:\Users\Dominikus\GitTHM\pis\SoSe2019\tictactoe>gradle run

> Task :run
Hello world.

BUILD SUCCESSFUL in 3s
2 actionable tasks: 2 executed
~~~


