---
author: Dominikus Herzberg, Jonas Reitz
---

# Javalin: HTML-Forms <!-- omit in toc -->

Dieses Beispiel ist der Javalin-Dokumentation entnommen ("[Working with HTML forms and a Java backend](https://javalin.io/tutorials/html-forms-example)"). Der Quellcode ist geringfügig angepasst,in Kotlin übersetzt und von mir (Herzberg) ausführlich kommentiert (hier das originale [GitHub-Repo](https://github.com/tipsy/javalin-html-forms-example)) und wird mit Gradle statt mit Maven zum Ausführung gebracht.

- [Anwendung kompilieren und starten](#anwendung-kompilieren-und-starten)
- [Der Aufbau der Anwendung](#der-aufbau-der-anwendung)
  - [Die HTML-Datei](#die-html-datei)
  - [Der Code zum Webserver](#der-code-zum-webserver)
  - [Schauen, was passiert](#schauen-was-passiert)
- [Den Webserver hacken!](#den-webserver-hacken)

## Anwendung kompilieren und starten

Eine Gradle-Datei ist wesentlich leichter zu lesen als ihr Maven-Pendant. Die gezeigte Datei `build.gradle` liest sich fast wie Klartext:
 
* Es geht um eine Kotlin-Applikation,
* die abhängt von der Javalin-Bibliothek und einer Logging-Bibliothek namens `slf4j`; die Bibliothekspakete werden von Gradle automatisch heruntergeladen
* und der Programmeinstieg befindet sind in der Klasse `App`, welche sich im Paket `HTMLFormsExampleKotlin` befinded.

~~~ gradle
plugins {
    // Apply the Kotlin JVM plugin to add support for Kotlin.
    id 'org.jetbrains.kotlin.jvm' version '1.3.61'
    
    // Apply the application plugin to add support for building a CLI application.
    id 'application'
}

repositories {
    jcenter()
}

dependencies {
    // Align versions of all Kotlin components
    implementation platform('org.jetbrains.kotlin:kotlin-bom')
    
    // Use the Kotlin JDK 8 standard library.
    implementation 'org.jetbrains.kotlin:kotlin-stdlib-jdk8'
    
    // Add Javalin dependency
    compile group: 'io.javalin', name: 'javalin', version: '3.7.0'
    
    // Add slf4j Logger dependency
    compile "org.slf4j:slf4j-simple:1.7.28"
}

application {
    // Define the main class for the application.
    mainClassName = 'HTMLFormsExampleKotlin.AppKt'
}

~~~

Um die Anwendung als Webserver zu starten, geben Sie `gradle run` ein; Achtung, der erste Aufruf von `gradle` dauert in der Regel etwas. Die Ausführung beenden Sie mit `Strg` + `C`. Mit Hilfe eines Browsers treten in Interaktion mit der Anwendung indem Sie in der Adresszeile `localhost:7070` eingeben.  

## Der Aufbau der Anwendung

Die hier betrachtete Anwendung besteht aus

* einer HTML-Datei, in die CSS-Angaben eingebettet sind, und
* dem Code der Anwendung

Mit der Anwendung kann man eine Reservierung in einem Restaurant vornehmen für Samstag oder Sonntag und zwar entweder für 20 oder 21 Uhr. Und man kann die Uhrzeit einer Reservierung für einen gegebenen Tag (Samstag oder Sonntag) abfragen.

Zu Demozwecken ist noch gezeigt, wie man eine Datei beim Server hochladen kann. So schön schlank der Code ist, seit der [Verabschiedung der Urheberrechtsreform durch das EU-Parlament]( https://heise.de/-4350043) ist die Frage, was Sie mit den Uploads machen ;-) 

### Die HTML-Datei

Die folgende HTML-Datei mit dem Namen `index.html` liegt im Ordner `main/resources/public`. Sie ist durch Kopfzeilen (_header_) in den Ebenen `<h1>` und `<h2>` strukturiert und enthält drei verschiedene Eingabemasken (_forms_). Am Ende der Datei sind Formatierungsangaben als CSS per `<style>` eingebettet; sie bestimmen, in welcher Schrift der Text gesetzt werden soll und wie breit Ränder (_margins_) sind

~~~ html
<h1>Reserve a table at our fancy restaurant</h1>

<h2>Make reservation:</h2>
<form method="post" action="/make-reservation">
    Choose day
    <select name="day">
        <option value="saturday">Saturday</option>
        <option value="sunday">Sunday</option>
    </select>
    <br>
    Choose time
    <select name="time">
        <option value="8:00 PM">8:00 PM</option>
        <option value="9:00 PM">9:00 PM</option>
    </select>
    <br>
    <button>Submit</button>
</form>


<h2>Check your reservation:</h2>
<form method="get" action="/check-reservation">
    Choose day
    <select name="day">
        <option value="saturday">Saturday</option>
        <option value="sunday">Sunday</option>
    </select>
    <br>
    <button>Submit</button>
</form>


<h1>Upload example</h1>
<form method="post" action="/upload-example" enctype="multipart/form-data">
    <input type="file" name="files" multiple>
    <button>Submit</button>
</form>

<style>
    body {
        font-family: Arial, sans-serif;
        max-width: 600px;
        margin: 40px auto;
    }

    select {
        margin: 5px 0;
    }

    h1, h2, h3 {
        margin-top: 1.5em;
        margin-bottom: 5px;
    }
</style>
~~~

Man kann sehr gut den HTML-Code mit dem Erscheinungsbild der Webseite in Verbindung bringen. So sieht die HTML-Seite im Webbrowser aus:

![Die HTML-Seite gerendert durch den Browser](bilder/Client.png)

In den Eingabemasken (`<form>`) wird außerdem angegeben, was passiert, wenn man den `<button>` drückt.

Ein Blick auf die Eingabemaske zu "Make reservation":

* Die Eingabemaske überträgt die Eingabedaten mit der Methode `post` als Aktion `make-reservation` an den Server
* Als Eingabedaten werden zwei Wertpaare übertragen:
  * `day`/`saturday` bzw. `day`/`sunday` und
  * `time`/`8:00 PM` bzw. `time`/`9:00 PM`
  
### Der Code zum Webserver

Der Code ist für Sie kommentiert.

~~~ kotlin
import io.javalin.Javalin
import io.javalin.core.util.FileUtil
import io.javalin.http.UploadedFile

class App {


    /* Die Map `reservations` wird mit zwei Key/Value-Paaren initialisiert.
       Das sind die Daten, die der Server verwaltet. Da die Daten nicht
       in einer Textdatei oder in einer Datenbank gespeichert werden,
       gehen sie mit dem Beenden des Servers verloren.
    */
    private var reservations  = mutableMapOf<String,String>(
            "saturday" to "No reservation",
            "sunday" to "No reservation"
    )

    init {
        /* Webserver aufsetzen: https://javalin.io/documentation#server-setup
           - mit direktem Zugriff auf Dateien in `/resources/public`,
             wobei `index.html` bei `http://{host}:{port}/` ausgeliefert wird
           - mit Port 7070 als Eingang für HTTP-Requests, wobei der Webserver
             Anfragen synchron verarbeitet, d.h. während der Verarbeitung
             keine weiteren Anfragen gleichzeitig bearbeiten kann.
        */

        val app = Javalin.create{config ->
            config.addStaticFiles("/public")
        }.start(7070)

        /* Für `app` werden mehrere Reaktionen auf HTTP-Anfragen eingerichtet.

           `.post()` und `.get()` sind sogenannte _Endpoint Handler_ (siehe
           https://javalin.io/documentation#endpoint-handlers) für die
           HTTP-Anfragemethoden `POST` und `GET`.

           - `POST` dient zur Übermittlung meist größerer Datenmengen
           - `GET` soll laut Standard nur Daten abrufen, wobei Argumente in
              der URI mitgeliefert werden können

           Die Endpoint Handler können einen Pfad enthalten.
           Das `Context`-Objekt (https://javalin.io/documentation#context)
           enthält alles, um eine HTTP-Anfrage zu behandeln (_request methods_)
           und um eine HTTP-Antwort aufzusetzen (_response methods_).

           Hier wird per Lambda-Ausdruck der Umgang mit der HTTP-Anfrage
           programmiert.

        */
        /* Die Reservierung mit den Daten aus Form-Parametern werden in der
           Map abgelegt mit der Tagesangabe (ermittelt über den Parameter
           `day`) als Schlüssel und der Uhrzeitangabe (ermittelt über `time`)
           als Wert.
           Da ctx.formParam() einen nullable String zurück gibt,
           müssen wir diesen noch über die toString() funktion zu einem String machen.
           Eine andere Möglichkeit wäre, die Map als mutableMapOf<String?,String?> initialisieren zu lassen.
        */

        app.post("/make-reservation"){ ctx ->

            reservations[ctx.formParam("day").toString()] = ctx.formParam("time").toString()
            ctx.html("Your reservation has been saved")
        }

        /* Die für einen Tag hinterlegte Uhrzeit wird abgefragt.
         */
        app.get("/check-reservation"){ ctx ->
            //Only runs .html() if reservations[ctx.queryParam] is not null
            reservations[ctx.queryParam("day")]?.let { ctx.html(it) }
        }

        /* Die mit der POST-Methode übertragenen Dateien werden im
           Projekt-Ordner `/upload` abgelegt. Falls dieser Ordner noch nicht existiert,
           wird er automatisch erstellt.
        */
        app.post("/upload-example") { ctx ->
            ctx.uploadedFiles("files").forEach { uploadedFile: UploadedFile ->
                FileUtil.streamToFile(uploadedFile.content,"upload/"+uploadedFile.filename)
            }
        }

    }

}
/* Der Einstiegspunkt des Programmes, welche eine neue Instanz der App erstellt.
 */
fun main(args: Array<String>) {
    App();
}
~~~

Sobald Sie einen `<button>` drücken wird eine HTTP-Anfrage an den Webserver gesendet, im Beispiel entweder als `POST`- oder als `GET`-Methode.

Wenn Sie die Anwendung mit `gradle run` ans Laufen bringen, geben Sie in Ihrem Browser `localhost:7070` ein, um die Anwendung zu sehen. Ihnen wird, wie im Code beschrieben, die `index.html`-Seite angezeigt.

### Schauen, was passiert

im Chrome-Browser können Sie über das Menü oder die Tastenkombination `Strg` + `Shift` + `I` die Entwicklungswerkzeuge zu der Webseite aktivieren.

Drücken Sie in der ersten Eingabemaske die Submit-Schaltfläche. Sie erhalten den Text "Your reservation has been saved" zurück, so wie er im Code als Antwort auf die HTTP-Anfrage hinterlegt ist.

Wenn Sie sich unter dem Eintrag "Network" die "Headers" für die Anfrage `make-reservation` anschauen, dann sehen Sie sehr detailliert, was der Browser tatsächlich alles an Daten an den Javalin-Server geschickt hat. Wenn Sie ganz nach unten scrollen (im Bild nicht zu sehen), dann erscheint der Bereich der `Form Data` mit den übertragenen Daten.

![Netzwerkanalyse der HTTP-Anfrage](bilder/NetworkHeader.png)

Mit diesen im Browser eingebauten Werkzeugen können Sie sehr gut nachvollziehen, was der Browser an den Webserver geschickt und was der Browser als Antwort erhalten hat.

## Den Webserver hacken!

Das interessante ist, dass man mit der HTML-Seite `index.html` zwar über die Eingabemasken HTTP-Anfragen generiert, aber der Webserver akzeptiert durchaus auch andere Daten, wenn es der Code nicht verbietet. Man kann den Webserver auch ganz anders mit Requests versorgen, z.B. mit `curl` (https://curl.haxx.se/), einem Werkzeug für die Kommandozeile, mit dem man HTPP-Anfragen absetzen kann.

> Installieren Sie `curl` unter Windows am besten mit `scoop`!

Wir befragen, ob der Webserver eine Reservierung für Samstag vorliegen hat. Die Antwort entspricht dem initialisierten Wert in der Map `reservations`.

~~~ shell
>curl localhost:7070/check-reservation?day=saturday
No reservation
~~~

Fällt Ihnen was auf? Diese Interaktion kollidiert nicht mit der, die wir im Webbrowser gestartet haben! Jeder Reiter (Tab) im Browser oder jede Kommandozeile hat ihren eigenen Anwendungskontext mit dem Server!

`curl` erkennt von selbst, dass es sich um eine `GET`-Methode für die HTTP-Anfrage handelt. Man hätte auch ausführlicher schreiben können:

~~~ shell
>curl -X GET localhost:7070/check-reservation?day=saturday
No reservation
~~~

Nehmen wir nun eine Reservierung vor, die in der HTML-Eingabemaske gar nicht vorgesehen war; `-d` ist die Kurzform für `--data` und enthält die Daten der `POST`-Methode.

~~~ shell
>curl localhost:7070/make-reservation -d "day=Montag&time=kurz+nach+acht"
Your reservation has been saved
~~~

Wir haben dem Webserver an der HTML-Datei vorbei ganz andere Daten untergeschoben. Und es hat funktioniert:

~~~ shell
>curl -X GET localhost:7070/check-reservation?day=Montag
kurz nach acht
~~~

Genau so versucht man sich beim Hacken eines Webservers! Was lernen wir daraus?

> Ein sicherer Webserver muss überprüfen, ob er Daten bekommt, mit denen er arbeiten möchte!
 



