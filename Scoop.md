---
author: Dominikus Herzberg
---

# Scoop zur Installation von Software <!-- omit in toc -->

Um sich die Installation der benötigten Softwarepakete zu vereinfachen, empfehle ich [Scoop](https://scoop.sh/) für Windows und [Homebrew](https://brew.sh/index_de.html) für macOS, das auch nutzbar ist für Linux bzw. das Windows Subsystem for Linux. Scoop ist insbesondere gedacht für die Installation von Werkzeugen, die über die Kommandozeile genutzt werden. Graphische Anwendungen, wie z.B. einen Browser oder eine Entwicklungsumgebung, sollten Sie wie gewohnt von der Webseite zur Anwendung herunterladen und installieren.

- [In Kürze: Der Umgang mit Scoop](#in-kürze-der-umgang-mit-scoop)
- [Ein Beispiel: Die Installation von Java](#ein-beispiel-die-installation-von-java)

## In Kürze: Der Umgang mit Scoop

Der große Vorteil von Scoop liegt darin, dass Sie sich nicht um das Setzen von Umgebungsvariablen kümmern müssen (eine Herausforderung, an der man manchmal verzweifeln kann) und dass es sehr leicht ist, Pakete zu aktualisieren oder sie gar wieder zu löschen.

Scoop ist einfach zu bedienen. Öffnen Sie nach der Installation von Scoop eine Kommandozeile. Mit Drücken der `<Windows>`-Taste + `R` (für _run_) werden Sie nach dem Kommando zur Ausführung gefragt. Geben Sie `cmd` ein und die Kommandozeile öffnet sich. Wenn Sie nun `scoop` eingeben, bekommen Sie eine Kurzübersicht über die Scoop-Befehle. Das ist fast selbsterklärend, oder?

~~~ text
>scoop
Usage: scoop <command> [<args>]

Some useful commands are:

alias       Manage scoop aliases
bucket      Manage Scoop buckets
cache       Show or clear the download cache
checkup     Check for potential problems
cleanup     Cleanup apps by removing old versions
config      Get or set configuration values
create      Create a custom app manifest
depends     List dependencies for an app
export      Exports (an importable) list of installed apps
help        Show help for a command
home        Opens the app homepage
info        Display information about an app
install     Install apps
list        List installed apps
prefix      Returns the path to the specified app
reset       Reset an app to resolve conflicts
search      Search available apps
status      Show status and check for new app versions
uninstall   Uninstall an app
update      Update apps, or Scoop itself
virustotal  Look for app's hash on virustotal.com
which       Locate a shim/executable (similar to 'which' on Linux)


Type 'scoop help <command>' to get help for a specific command.
~~~

Die Informationen für die Installationspakete sind in öffentlich zugänglichen Git-Verzeichnissen abgelegt, die Scoop "Eimer" (_bucket_) nennt.

* https://github.com/lukesampson/scoop/tree/master/bucket, _main bucket_
* https://github.com/lukesampson/scoop-extras, _extra bucket_

Darüber hinaus stehen weitere _Buckets_ zur Verfügung, wie z.B. für die Installation von Java. Sie können sich die Liste aller _Buckets_ (die _bucket list_) entweder im [Git-Repo](https://github.com/lukesampson/scoop/blob/master/buckets.json) anschauen oder Scoop um Auskunft bitten.

~~~
C:\Users\Dominikus>scoop bucket known
extras
versions
nightlies
nirsoft
php
nerd-fonts
nonportable
java
games
jetbrains
~~~

Man kann sich, wenn man im Zweifel über die Herkunft eines Softwarepackets ist, die json-Dateien in den jeweiligen _Buckets_ anschauen, um festzustellen, aus welchen Quellen Scoop die Software installiert und welche Befehle zum Einsatz kommen.

Ähnlich wie Scoop ist Homebrew organisiert, die Liste aller Pakete ist einzusehen unter

* https://formulae.brew.sh/formula/

## Ein Beispiel: Die Installation von Java

Beispielhaft zeige ich Ihnen, wie Sie Java mit Hilfe von Scoop installieren.

Eine kleine Vorbemerkung: Wenn Sie Scoop zum ersten Mal für die Installation eines Softwarepakets nutzen, müssen Sie `scoop install git` in der Kommandozeile eingeben, damit Scoop überhaupt in der Lage ist, die weiteren Pakete mittels `git` herunterzuladen und zu installieren. Scoop wird Sie darauf gegebenenfalls aufmerksam machen.

Da das Java Development Kit (JDK) nicht im _main bucket_ zur Verfügung steht, mache ich mich auf die Suche, wo das JDK zu finden ist. Es gibt den Java-Bucket, und der bietet gleich eine Vielzahl an JDKs an. Die Liste, die Sie in ihrer Konsole sehen, ist sicher aktueller als hier gezeigt.

~~~
C:\Users\Dominikus>scoop search jdk
Results from other known buckets...
(add them using 'scoop bucket add <name>')

'java' bucket:
    adoptopenjdk-hotspot-jre (15.0.2-7)
    adoptopenjdk-hotspot (15.0.2-7)
    adoptopenjdk-lts-hotspot-jre (11.0.10-9)
    adoptopenjdk-lts-hotspot (11.0.10-9)
    adoptopenjdk-lts-openj9-jre (11.0.10-9-0.24.0)
    adoptopenjdk-lts-openj9-xl-jre (11.0.10-9-0.24.0)
    adoptopenjdk-lts-openj9-xl (11.0.10-9-0.24.0)
    adoptopenjdk-lts-openj9 (11.0.10-9-0.24.0)
    adoptopenjdk-lts-upstream-jre (11.0.10-9)
    adoptopenjdk-lts-upstream (11.0.10-9)
    adoptopenjdk-openj9-jre (16-36-0.25.0)
    adoptopenjdk-openj9 (16-36-0.25.0)
    graalvm-annual-jdk11 (21.0.0.2)
    graalvm-annual-jdk8 (21.0.0.2)
    graalvm-jdk11 (21.0.0.2)
    graalvm-jdk8 (21.0.0.2)
    graalvm-lts-jdk11 (20.3.1.2)
    graalvm-lts-jdk8 (20.3.1.2)
    graalvm-nightly-jdk11 (21.1.0-dev-20210406_2231)
    graalvm-nightly-jdk8 (21.1.0-dev-20210406_2231)
    liberica16-full-jdk (16-36)
    liberica16-jdk (16-36)
    liberica16-lite-jdk (16-36)
    ojdkbuild-full (14.0.2.12-1)
    ojdkbuild (14.0.2.12-1)
    ojdkbuild10-full (10.0.2-1.b13)
    ojdkbuild10 (10.0.2-1.b13)
    ojdkbuild11-full (11.0.10.9-1)
    ojdkbuild11 (11.0.10.9-1)
    ojdkbuild12-full (12.0.2.9-1)
    ojdkbuild12 (12.0.2.9-1)
    ojdkbuild13-full (13.0.3.3-1)
    ojdkbuild13 (13.0.3.3-1)
    ojdkbuild14-full (14.0.2.12-1)
    ojdkbuild14 (14.0.2.12-1)
    ojdkbuild8-full (1.8.0.282-1.b08)
    ojdkbuild8 (1.8.0.282-1.b08)
    ojdkbuild9-full (9.0.4-1.b11)
    ojdkbuild9 (9.0.4-1.b11)
    openjdk-ea (17-16-ea)
    openjdk (16-36)
    openjdk10 (10.0.2-13)
    openjdk11 (11.0.2-9)
    openjdk12 (12.0.2-10)
    openjdk13 (13.0.2-8)
    openjdk14 (14.0.2-12)
    openjdk15 (15.0.2-7)
    openjdk16 (16-36)
    openjdk17 (17-16-ea)
    openjdk7-unofficial (7u80-b32)
    openjdk8-redhat-jre (8u282-b08)
    openjdk8-redhat (8u282-b08)
    openjdk9 (9.0.4-12)
    sapmachine16-jdk (16)
    sapmachine17-jdk (17-9-ea)
    zulu16-jdk (16.28.11)
    zulufx16-jdk (16.28.11)
~~~

Fügen Sie den Java-Bucket zu Scoop hinzu.

~~~
C:\Users\Dominikus>scoop bucket add java
Checking repo... ok
The java bucket was added successfully.
~~~

Nun müssen Sie sich entscheiden, welche Installation für das JDK Sie wählen. Von welchem Anbieter und zu welcher Sprachversion von Java möchten Sie das Paket beziehen?

> Ich empfehle Ihnen das [OpenJDK](https://de.wikipedia.org/wiki/OpenJDK), da die restriktive Lizenzpolitik von Oracle Ihnen Steine in den Weg legt, sobald sich auch nur geringste kommerzielle Aspekte im Gebrauch des OracleJDK andeuten.

Mit Scoop ist die Installation rasch und unkompliziert erledigt. Wenn Sie keine spezielle Version auswählen, wird die aktuellste, verfügbare gewählt.

~~~ shell
C:\Users\Dominikus>scoop install openjdk
~~~

Falls Sie das OpenJDK mit Scoop bereits installiert haben, aktualisieren Sie das JDK mit dem `update`-Befehl.

~~~ shell
C:\Users\Dominikus>scoop update openjdk
~~~

Nur eines müssen Sie noch wissen: In Windows werden die Umgebungsvariablen in der aktuellen Kommandozeile leider nicht zuverlässig aktualisiert. Man muss eine neue Kommandozeile öffnen (siehe oben). Und dann können Sie leicht überprüfen, ob Java wie geplant bereitsteht.

~~~ shell
C:\Users\Dominikus>java --version
openjdk 16 2021-03-16
OpenJDK Runtime Environment (build 16+36-2231)
OpenJDK 64-Bit Server VM (build 16+36-2231, mixed mode, sharing)
~~~

Viel Erfolg mit der Installation weiterer Softwarepakete!

